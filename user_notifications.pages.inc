<?php
// $Id$

/**
 * @file
 * Menu callbacks
 */

/**
 * List notifications
 *
 * @TODO - Help me group the results in the query
 */
function user_notifications_list_page() {
  global $user;
  $account = $user;
	
	$result = user_notifications_display_query($account);
	$headers = array();
	while ($notification = db_fetch_object($result)) {
		$header = theme('user_notifications_date_header', $notification->timestamp);
		$row = '';
		if (empty($headers[$header])) {
		  $row = $header;
			$headers[$header] = $header;
		}
		$row .= theme('user_notifications_notification', $notification);
		$rows[] = $row;
		$notifications[$notification->nid] = $notification;
	}
	
	//markup these notifications as read after rendering
	user_notifications_markasread($notifications);
	
	//pass to stub theme function to allow overrides
  return theme('user_notifications_notification_list', $account, $rows, TRUE);
}
