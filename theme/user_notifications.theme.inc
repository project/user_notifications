<?php
// $Id$

/**
 * @file
 * Themes hooks and callback functions
 */
 
function theme_user_notifications_unread_count_link($link_text = '', $hide_empty = FALSE) {
	$count = user_notifications_unread_count();
	if (empty($count) && $hide_empty) {
		return;
	}
	
	$inbox = $link_text ? $link_text : t('Notifications');
	if ($count > 0) {
		$inbox .= ' (<strong>'. format_plural($count, '1 new', '@count new') .'</strong>)';
	}
	return l($inbox, 'user-notifications', array('html' => true, 'attributes' => array('title' => t("Notifications and messages from other users"))));
}

/**
 * themes date headers on notifications page
 */
function theme_user_notifications_date_header($date, $format = 'D jS M Y') {
	return '<h3 class="notification-date-header">'. format_date($date, 'custom', $format) .'</h3>';
}

/**
 * Render $message array
 */
function template_preprocess_user_notifications_notification(&$vars) {
	$notification = $vars['notification'];
	
	$vars['classes_array'] = array();
	if ($notification->is_new) {
		$vars['classes_array'][] = 'notification-is-new';
	}
  if ($notification->tag) {
		$vars['classes_array'][] = 'notification-'. str_replace('_', '-', trim(strtolower($notification->tag)));
	}	
	
	$vars['classes'] = !empty($vars['classes_array']) ? ' '. implode(' ', $vars['classes_array']) : '';
	$vars['message']['time'] = format_date($notification->timestamp, 'custom', 'g:iA');
	$vars['message']['subject'] = $notification->subject;
	$vars['message']['body'] = check_markup($notification->body, $notification->format);
	
	//template suggestions depending on $vars['display']
	$vars['template_files'][] = 'user-notifications-notification-'. $vars['display'];
}

/**
 * stub theme function to allow overrides
 * 
 * @param $account
 *  User account object the notifications related to
 * @param $rows
 *   An array of the notifications (array gives more display options)
 * @param $page
 *  If TRUE its a page display, FALSE is a block display
 * 
 * @see - user_notifications.pages.inc
 */
function theme_user_notifications_notification_list($account, $rows = array(), $page = TRUE) {
	if (!empty($rows)) {
	  $output = implode('', $rows);
	  if ($page) {
		  $output .= theme('pager');
		}
	}
	else {
		$output = '<div class="no-notifications">'. t('No notifications to show.') .'</div>';
	}
	$output .= l(t('Notification Settings'), 'user/'. $account->uid .'/edit/user_notifications');
	if (!$page) {
		$output .= theme('more_link', url('user-notifications'), t('All Notification'));
	}
	return $output;
}

/**
 * outputs notifications list in block
 * 
 * @param $account
 *  User account object the notifications related to
 * 
 * @see - user_notifications.pages.inc
 */
function theme_user_notifications_notification_block($account = '', $limit = '5', $markasread = FALSE) {
  global $user;
  if (empty($account)) {
	  $account = $user;
	}
	
	$result = user_notifications_display_query($account, FALSE, FALSE, $limit);
	$headers = array();
	while ($notification = db_fetch_object($result)) {
		$header = theme('user_notifications_date_header', $notification->timestamp);
		$row = '';
		if (empty($headers[$header])) {
		  $row = $header;
			$headers[$header] = $header;
		}
		$row .= theme('user_notifications_notification', $notification, 'block');
		$rows[] = $row;
		$notifications[$notification->nid] = $notification;
	}
	
	if ($markasread) {
    //markup these notifications as read after rendering
	  user_notifications_markasread($notifications);	
	}
	
	return theme('user_notifications_notification_list', $account, $rows, FALSE);
}
