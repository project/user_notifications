<?php
// $Id$

/**
 * @file
 * Admin callbacks for the user noitifications module.
 */

/**
 * return the system settings form
 */
function user_notifications_settings() {
  $form = array();

  $form['flush_deleted'] = array(
    '#type'        => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed'   => FALSE,
    '#title'       => t('Flush deleted messages'),
    '#description' => t('You can optionally use the cron run to flush out old notifications. Notifications are permanently deleted so use with caution.'),
  );

  $form['flush_deleted']['user_notifications_flush_enabled'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Flush old notifications'),
    '#default_value' => variable_get('user_notifications_flush_enabled', FALSE),
    '#description'   => t('Enable the flushing of old notifications. Requires that cron is enabled'),
  );

  $form['flush_deleted']['user_notifications_flush_days'] = array(
    '#type' => 'select',
    '#title' => t('Flush notifications older than this many days'),
    '#default_value' => variable_get('user_notifications_flush_days', 30),
    '#options' => drupal_map_assoc(array(0, 1, 2, 5, 10, 30, 100)),
  );

  $form['flush_deleted']['user_notifications_flush_max'] = array(
    '#type' => 'select',
    '#title' => t('Maximum number of notifications to flush per cron run'),
    '#default_value' => variable_get('user_notifications_flush_max', 200),
    '#options' => drupal_map_assoc(array(50, 100, 200, 500, 1000)),
  );

  return system_settings_form($form);
}
